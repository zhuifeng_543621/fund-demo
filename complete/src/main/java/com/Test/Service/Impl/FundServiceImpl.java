package com.Test.Service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.Test.DAO.FundDAO;
import com.Test.Model.Fund;
import com.Test.Service.FundService;

public class FundServiceImpl implements FundService {

	@Autowired
	private FundDAO fundDAO;

	@Override
	public List<Fund> fundsList() {
		List<Fund> fundList = fundDAO.findFunds();
		return fundList;
	}

	@Override
	public void addFund(Fund fund) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Double> historyPrice(String fundName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateFund(Fund fund) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<String> namesFund() {
		List<String> namesList = new ArrayList<String>();
		for (Fund fund : fundsList()) {
			namesList.add(fund.getFundName());
		}
		return namesList;
	}

	@Override
	public void addFunds(List<Fund> funds) {
		// TODO Auto-generated method stub
	}

}
