package com.Test.Model;

public class InvestmentItemInfo {
	private String fundName;
	private double savingNumber;
	private double savingTotal;
	private double onlinePrice;
	private double withdrawTotal;
	private InvestmentItem investItem;

	public InvestmentItemInfo() {

	}

	public InvestmentItemInfo(InvestmentItem investItem) {
		this.investItem = investItem;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public double getSavingNumber() {
		return savingNumber;
	}

	public void setSavingNumber(double savingNumber) {
		this.savingNumber = savingNumber;
	}

	public double getSavingTotal() {
		return savingTotal;
	}

	public void setSavingTotal(double savingTotal) {
		this.savingTotal = savingTotal;
	}

	public double getOnlinePrice() {
		return onlinePrice;
	}

	public void setOnlinePrice(double onlinePrice) {
		this.onlinePrice = onlinePrice;
	}

	public double getWithdrawTotal() {
		return withdrawTotal;
	}

	public void setWithdrawTotal(double withdrawTotal) {
		this.withdrawTotal = withdrawTotal;
	}

	public InvestmentItem getInvestItem() {
		return investItem;
	}

	public void setInvestItem(InvestmentItem investItem) {
		this.investItem = investItem;
	}
}
