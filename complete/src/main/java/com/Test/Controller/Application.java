package com.Test.Controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.Test.DAO.FundDAO;
import com.Test.DAO.InvestmentItemDAO;
import com.Test.DAO.Impl.FunDAOJsonImpl;
import com.Test.DAO.Impl.InvestmentItemDAOImpl;
import com.Test.Service.FilterService;
import com.Test.Service.FundService;
import com.Test.Service.PService;
import com.Test.Service.Impl.FilterServiceImpl;
import com.Test.Service.Impl.FundServiceImpl;
import com.Test.Service.Impl.InvestmentInfoServiceImpl;
import com.Test.Service.Impl.PServiceImpl;
import com.Test.Utils.FundUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    @Bean
    public FundDAO getFundDAO(){
    	return new FunDAOJsonImpl(FundUtils.dataPath);
    }
    
    @Bean
    public InvestmentItemDAO getInvestItemDAO(){
    	return new InvestmentItemDAOImpl(FundUtils.dataPathItem);
    }
    
    @Bean
    public FundService getFundService(){
    	return new FundServiceImpl();
    }
    
    @Bean
    public FilterService getFilterService(){
    	return new FilterServiceImpl();
    }
    
    @Bean
    public PService getPService(){
    	return new PServiceImpl();
    }
    @Bean
    public ObjectMapper getMapper(){
    	return new ObjectMapper();
    }
    
    
    @Bean
    public InvestmentInfoServiceImpl getinvestItemDAO(){
    	return new InvestmentInfoServiceImpl();
    }
}
