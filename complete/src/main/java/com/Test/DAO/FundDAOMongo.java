package com.Test.DAO;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.Test.Model.Fund;

public interface FundDAOMongo extends MongoRepository<Fund, String> {

}
