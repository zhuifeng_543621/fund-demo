package com.Test.Model;

import java.util.List;

public class DataFilters {
	private List<String> fSearchKeys;
	private List<String>fOrderKeys;
	public List<String> getfSearchKeys() {
		return fSearchKeys;
	}
	public void setfSearchKeys(List<String> fSearchKeys) {
		this.fSearchKeys = fSearchKeys;
	}
	public List<String> getfOrderKeys() {
		return fOrderKeys;
	}
	public void setfOrderKeys(List<String> fOrderKeys) {
		this.fOrderKeys = fOrderKeys;
	}
}
