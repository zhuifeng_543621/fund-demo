package com.Test.DAO.Impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.Test.DAO.InvestmentItemDAO;
import com.Test.Model.InvestmentItem;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class InvestmentItemDAOImpl implements InvestmentItemDAO{
	
	private ObjectMapper mapper;

	private String dataPath;

	private List<InvestmentItem> investItems;
	
	public InvestmentItemDAOImpl(String dataPath){
		mapper=new ObjectMapper();
		this.dataPath= dataPath;
		readIvestItems();
	}

	@Override
	public void add(InvestmentItem investItem) {
		investItem.setItemId(investItems.size());
		investItems.add(investItem);
	}

	@Override
	public void delete(InvestmentItem investItem) {
	}

	@Override
	public void update(InvestmentItem investItem) {
		InvestmentItem item=findbyId(investItem.getFundId());
		if(null!=item){
			item.update(investItem);
		}
		
	}

	@Override
	public List<InvestmentItem> findSubById(int itemId) {
		InvestmentItem item=findbyId(itemId);
		if(null==item){
			return null;
		}
		List<InvestmentItem> list=new ArrayList<InvestmentItem>();
		for (InvestmentItem investmentItem : investItems) {
			if(investmentItem.getMainTypeId()==0 &&investmentItem.getSubFundId()==item.getSubFundId()){
				list.add(investmentItem);
			}
		}
		return list;
	}

	@Override
	public List<InvestmentItem> findMainAll() {
		List<InvestmentItem> list=new ArrayList<InvestmentItem>();
		for (InvestmentItem investmentItem : investItems) {
			if(investmentItem.getMainTypeId()==1){
				list.add(investmentItem);
			}
		}
		return list;
		
	}
	
	public void readIvestItems() {
		try {
			InvestmentItem[] arr = mapper.readValue(new File(dataPath), InvestmentItem[].class);
			investItems = Arrays.asList(arr);
			System.out.println(investItems);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public InvestmentItem findbyId(int itemId) {
		if(itemId>=investItems.size()){
			return null;
		}
		return investItems.get(itemId);
	}
	
	public void commit() {
		try {
			mapper.writeValue(new File(dataPath), investItems);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int findSavingNumberByMainId(int itemId) {
		int savingNum=0;
		for (InvestmentItem investmentItem : findSubById(itemId)) {
			savingNum+=investmentItem.getNumbers();
		}
		return savingNum;
	}

	@Override
	public double findWithdrawByMainId(int itemId) {
		double withDraw=0;
		for (InvestmentItem investmentItem : findSubById(itemId)) {
			withDraw+=investmentItem.computeTotal();
		}
		return withDraw;
	}
}
