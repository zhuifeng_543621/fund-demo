package com.Test.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.Test.DAO.InvestmentItemDAO;
import com.Test.Model.InvestmentItem;
import com.Test.Model.PModel;
import com.Test.Service.FilterService;
import com.Test.Service.FundService;
import com.Test.Service.InvestmentInfoService;
import com.Test.Service.PService;

public class PServiceImpl implements PService {
	@Autowired
	FundService fundService;
	
	@Autowired
	FilterService filterService;
	
	@Autowired
	InvestmentInfoService investInfoService;
	
	@Autowired
	private InvestmentItemDAO investItemReposity;
	
	@Override
	public PModel doProcess() {
		PModel model = new PModel();
		model.setFunds(fundService.fundsList());
		model.setFilters(filterService.getDataFilter());
		model.setInvestInfos(investInfoService.getInvestInfo());
		return model;
	}

	@Override
	public List<InvestmentItem> findInvestHistory(int itemId) {
		return investItemReposity.findSubById(itemId);
	}

}
