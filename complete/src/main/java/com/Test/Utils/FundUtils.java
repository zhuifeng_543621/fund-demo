package com.Test.Utils;

import java.util.ArrayList;
import java.util.List;

import com.Test.Model.Fund;
import com.Test.Model.InvestmentItem;

public class FundUtils {
	public static String dataPath = System.getProperty("user.dir")
			+ "\\src\\main\\resources\\text.json";
	public static String dataPathItem = System.getProperty("user.dir")
			+ "\\src\\main\\resources\\item.json";
	
	static List<Fund> funds = new ArrayList<Fund>();

	public static List<Fund> testFunds() {
		if (0 == funds.size()) {
			funds.add(new Fund("融通可转债", 1.32));
			funds.add(new Fund("易方达", 1.52));
			funds.add(new Fund("天弘中证", 2.32));
			funds.add(new Fund("汇添富", 4.02));
			funds.add(new Fund("汇添富", 6.02));
		}
		return funds;
	}
	
	static List<InvestmentItem> listItems=new ArrayList<InvestmentItem>();
	public static List<InvestmentItem> testInvesments() {
		if (0 == listItems.size()) {
			listItems.add(new InvestmentItem(0,1, 0, 0, 2.1, 100));
			listItems.add(new InvestmentItem(1,0, 0, 0, 3.0, 20));
			listItems.add(new InvestmentItem(2,0, 0, 0, 1.5, 20));
			listItems.add(new InvestmentItem(3,1, 1, 2, 0.8, 1000));
			listItems.add(new InvestmentItem(4,0, 1, 2, 1.0, 100));
			listItems.add(new InvestmentItem(5,0, 1, 2, 0.6, 600));
		}
		return listItems;
	}
}
