package com.Test.Service;

import java.util.List;

import com.Test.Model.Fund;

public interface FundService {
	List<Fund> fundsList();
	List<String> namesFund();
	void addFund(Fund fund);
	List<Double> historyPrice(String fundName);
	void updateFund(Fund fund);
	void addFunds(List<Fund> funds);
}
