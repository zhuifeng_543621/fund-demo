package com.Test.Service;

import com.Test.Model.DataFilters;

public interface FilterService {
	DataFilters getDataFilter();
}
