package com.Test.DAO.Impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.Test.DAO.FundDAO;
import com.Test.Model.Fund;
import com.Test.Utils.FundUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FunDAOJsonImpl implements FundDAO {
	private ObjectMapper mapper;

	private String dataPath;

	private List<Fund> funds;

	public void initiAdd(){
		funds=new ArrayList<Fund>();
		addFund(FundUtils.testFunds());
	}
	
	public FunDAOJsonImpl(String dataPath) {
		mapper=new ObjectMapper();
		this.dataPath=dataPath;
//		initiAdd();
		readFunds();
	}

	public void readFunds() {
		try {
			Fund[] arr = mapper.readValue(new File(dataPath), Fund[].class);
			funds = Arrays.asList(arr);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addFund(Fund fund) {
		fund.setFundID(funds.size());
		funds.add(fund);
	}

	@Override
	public void deleteFund(Fund fund) {
		// funds.remove(fund);
	}

	@Override
	public void updateFund(Fund fund) {
		Fund resultFund=findFundById(fund.getFundID());
		if(null!=resultFund){
			resultFund.updateFund(fund);
		}
	}

	@Override
	public List<Fund> findFunds() {
		return funds;
	}

	@Override
	public Fund findFundById(int fundId) {
		if (fundId >= funds.size()) {
			return null;
		}
		return funds.get(fundId);
	}

	public void commit() {
		try {
			mapper.writeValue(new File(dataPath), funds);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateFund(List<Fund> updateFunds) {
		for (Fund fund : updateFunds) {
			updateFund(fund);
		}
	}


	@Override
	public void addFund(List<Fund> funds) {
		for (Fund fund : funds) {
			addFund(fund);
		}
	}

	@Override
	public String fundNameById(int fundId) {
		Fund model=findFundById(fundId);
		return null==model?null:model.getFundName();
	}
}
