package com.Test.Service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.Test.Model.DataFilters;
import com.Test.Service.FilterService;
import com.Test.Service.FundService;

public class FilterServiceImpl implements FilterService {

	@Autowired
	FundService fundService;
	
	@Override
	public DataFilters getDataFilter() {
		DataFilters filter = new DataFilters();
		
		filter.setfSearchKeys(fundService.namesFund());
		
		List<String> orderKeys=new ArrayList<String>();
		orderKeys.add("fundPrice");
		filter.setfOrderKeys(orderKeys);
		
		return filter;
	}

}
