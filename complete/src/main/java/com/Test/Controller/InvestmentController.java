package com.Test.Controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.Test.Model.InvestmentItem;
import com.Test.Model.PModel;
import com.Test.Service.PService;

@RestController
public class InvestmentController {
	@Autowired
	PService service;
		
	@RequestMapping("/funds")
	@ResponseStatus(HttpStatus.OK)
	public PModel funds(HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		PModel model=service.doProcess();
		return model;
	}
	
	@RequestMapping("/historyItem/{itemId}")
	public List<InvestmentItem> findItems(@PathVariable int itemId,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		System.out.println("---------"+itemId);
		return service.findInvestHistory(itemId);
	}
	
}
