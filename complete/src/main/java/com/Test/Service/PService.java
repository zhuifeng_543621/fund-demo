package com.Test.Service;

import java.util.List;

import com.Test.Model.InvestmentItem;
import com.Test.Model.PModel;

public interface PService {
	PModel doProcess();
	List<InvestmentItem> findInvestHistory(int itemId);
}
