package com.Test.Model;

import java.util.Date;

public class InvestmentItem {
	private int itemId;
	private int mainTypeId;
	
	// 0 or 1
	private int subFundId;
	private int fundId;
	private Date date;
	private double currentPrice;
	private double numbers;
	
	public double computeTotal(){
		return currentPrice*numbers;
	}
	
	public InvestmentItem(){
		
	}

	public InvestmentItem(int mainTypeId, int subFundId, int FundId,
			double currentPrice, double numbers) {
		this(0, mainTypeId, subFundId, FundId, currentPrice, numbers);
	}
	
	public InvestmentItem(int itemId, int mainTypeId, int subFundId, int FundId,
			double currentPrice, double numbers) {
		this.mainTypeId = mainTypeId;
		this.subFundId = subFundId;
		this.fundId = FundId;
		this.currentPrice = currentPrice;
		this.numbers = numbers;
		this.itemId=itemId;
	}
	
	public void update(InvestmentItem item){
		this.currentPrice=item.getCurrentPrice();
		this.numbers=item.getNumbers();
	}

	public int getMainTypeId() {
		return mainTypeId;
	}

	public void setMainTypeId(int mainTypeId) {
		this.mainTypeId = mainTypeId;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	

	public int getSubFundId() {
		return subFundId;
	}

	public void setSubFundId(int subFundId) {
		this.subFundId = subFundId;
	}

	public int getFundId() {
		return fundId;
	}

	public void setFundId(int fundId) {
		this.fundId = fundId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public double getNumbers() {
		return numbers;
	}

	public void setNumbers(double numbers) {
		this.numbers = numbers;
	}

}
