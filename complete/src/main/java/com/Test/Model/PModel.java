package com.Test.Model;

import java.util.List;

public class PModel {
	private List<Fund> funds;
	private DataFilters filters;
	private List<InvestmentItemInfo> investInfos;
	
	public List<Fund> getFunds() {
		return funds;
	}
	public void setFunds(List<Fund> funds) {
		this.funds = funds;
	}
	public DataFilters getFilters() {
		return filters;
	}
	public void setFilters(DataFilters filters) {
		this.filters = filters;
	}
	public List<InvestmentItemInfo> getInvestInfos() {
		return investInfos;
	}
	public void setInvestInfos(List<InvestmentItemInfo> investInfos) {
		this.investInfos = investInfos;
	}
}
