package com.Test.Service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.Test.DAO.FundDAO;
import com.Test.DAO.InvestmentItemDAO;
import com.Test.Model.Fund;
import com.Test.Model.InvestmentItem;
import com.Test.Model.InvestmentItemInfo;
import com.Test.Service.InvestmentInfoService;

public class InvestmentInfoServiceImpl implements InvestmentInfoService {

	@Autowired
	private FundDAO fundReposity;
	
	@Autowired
	private InvestmentItemDAO investItemReposity;
	
	@Override
	public List<InvestmentItemInfo> getInvestInfo() {
		List<InvestmentItem> items=investItemReposity.findMainAll();
		List<InvestmentItemInfo> itemInfos=new ArrayList<InvestmentItemInfo>();
		InvestmentItemInfo itemInfo=null;
		for (InvestmentItem item : items) {
			itemInfo=new InvestmentItemInfo(item);
			int fundId=item.getFundId();
			Fund fund=fundReposity.findFundById(fundId);
			itemInfo.setFundName(fund.getFundName());
			itemInfo.setOnlinePrice(fund.getFundPrice());
			
			int investItemId=item.getItemId();
			int savingNum=investItemReposity.findSavingNumberByMainId(investItemId);
			itemInfo.setSavingNumber(savingNum);
			itemInfo.setSavingTotal(fund.getFundPrice()*savingNum);
			itemInfo.setWithdrawTotal(investItemReposity.findWithdrawByMainId(investItemId));
			
			itemInfos.add(itemInfo);
		}
		return itemInfos;
	}

}
