package com.Test.Model;

import org.springframework.data.annotation.Id;

public class FundModel {
	
	@Id
	private String id;
	
	private String fundName;
	private double fundPrice;
	
	public FundModel(){
		
	}
	
	public FundModel(String fundName,double price ){
		this.fundName=fundName;
		this.fundPrice=price;
	}
	
	@Override
	public String toString(){
		return this.fundName+"currrnt price: "+this.fundPrice;
	}
	
}
