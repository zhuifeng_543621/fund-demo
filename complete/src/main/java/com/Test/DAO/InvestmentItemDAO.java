package com.Test.DAO;

import java.util.List;

import com.Test.Model.InvestmentItem;

public interface InvestmentItemDAO {
	void add(InvestmentItem investItem);
	void delete(InvestmentItem investItem);
	void update(InvestmentItem investItem);
	List<InvestmentItem> findSubById(int itemId);
	List<InvestmentItem> findMainAll();
	InvestmentItem findbyId(int itemId);
	int findSavingNumberByMainId(int itemId);
	double findWithdrawByMainId(int itemId);
}
