package com.Test.DAO;

import java.util.List;

import com.Test.Model.Fund;

public interface FundDAO {
	void addFund(Fund fund);
	void addFund(List<Fund> funds);
	void deleteFund(Fund fund);
	void updateFund(Fund fund);
	void updateFund(List<Fund> funds);
	List<Fund> findFunds();
	Fund findFundById(int fundId);
	String fundNameById(int fundId);
}
