package com.Test.Model;

import java.util.ArrayList;
import java.util.List;

public class Fund {
	private int fundID;
	private String fundName;
	private double fundPrice;
	private boolean deleteFlag;
	List<Double> historyPrice = new ArrayList<Double>();

	public Fund() {
		this(null, 0);
	}

	public Fund(String fundName, double fundPrice) {
		this.fundName = fundName;
		this.fundPrice = fundPrice;
		this.historyPrice.add(fundPrice);
	}

	public void updateFund(Fund fund) {
		double newpPrice = fund.getFundPrice();
		this.fundPrice = newpPrice;
		this.historyPrice.add(newpPrice);
	}

	@Override
	public String toString() {
		return "Name: " + fundName + " Current price: " + fundPrice
				+ " History Price: " + historyPrice;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (null == obj)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Fund other = (Fund) obj;
		return this.fundID == other.getFundID() ? true : false;

	}

	public int getFundID() {
		return fundID;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundID(int fundID) {
		this.fundID = fundID;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public double getFundPrice() {
		return fundPrice;
	}

	public void setFundPrice(double fundPrice) {
		this.fundPrice = fundPrice;
	}

	public List<Double> getHistoryPrice() {
		return historyPrice;
	}

	public void setHistoryPrice(List<Double> historyPrice) {
		this.historyPrice = historyPrice;
	}

	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

}
