package com.Test.Service;

import java.util.List;

import com.Test.Model.InvestmentItemInfo;

public interface InvestmentInfoService {
	List<InvestmentItemInfo> getInvestInfo();
}
